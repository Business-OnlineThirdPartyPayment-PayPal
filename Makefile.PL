use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    'NAME'         => 'Business::OnlineThirdPartyPayment::PayPal',
    'VERSION_FROM' => 'PayPal.pm', # finds $VERSION
    'AUTHOR'       => 'Mark Wells <mark@freeside.biz>',
    'PREREQ_PM'    => { 
                        'Business::OnlineThirdPartyPayment' => 0,
                        'Crypt::CBC' => 0,
                        'Crypt::Blowfish' => 0,
                      },
);
